import menuqac.Prototype;
import menuqac.prototype.drink.ColdDrinks;
import menuqac.prototype.drink.HotDrinks;
import menuqac.prototype.food.mainDishes.Meats;
import menuqac.visitor.SauceVisitor.Sauce;
import menuqac.prototype.Food;

public aspect PrototypeAspect {

	public enum STATE{
		READY, PROCESSING, FREE
	}
	
	private STATE Prototype.state;
	
	public STATE Prototype.getState(){
		return this.state;
	}
	
	public String Prototype.getStateName(){
		switch (this.state) {
		case READY:
			return "Prêt";
		case PROCESSING:
			return "En préparation";
		case FREE:
			return "Préparer";
		}
		return null;
	}
	
	public boolean Prototype.isFree(){
		return this.state == STATE.FREE;
	}
	
	public boolean Prototype.isProcessing(){
		return this.state == STATE.PROCESSING;
	}
	
	public void Prototype.setState(STATE s){
		this.state=s;
	}
	
	public void Prototype.switchState(){
		switch (this.state) {
		case PROCESSING:
			this.state=STATE.READY;
			break;
		case FREE:
			this.state=STATE.PROCESSING;
			break;
		}
	}
	
	public String Prototype.cookerInformations(){
		String s = "<p style=\"text-align:left; color: black; font-weight:bold;\">" + this.getName() + "<br><p>" + this.getComment()
				+ "</p><br> <p style=\"text-align: right; color: black;\"></p></p>";
		return s;
	}
	
	public String ColdDrinks.cookerInformations(){
		String s = "<p style=\"text-align:left; color: black; display:inline-block; padding-top:3%; width:70%; font-weight:bold;\">" + this.getName() + "<p style=\"text-indent: 10%; display : block; margin: 1%;\">"
				+ (this.getIceCubes() ? "w icecubes" : "w/o icecubes") + "</p>"
				+ (this.getComment() == null ? "" : "<p style=\"text-indent: 10%; display : block; margin: 1%;\">" + this.getComment() + "</p>");
		return s;
	}
	
	public String HotDrinks.cookerInformations(){
		String s = "<p style=\"text-align:left; color: black; display:inline-block; padding-top:3%; width:70%; font-weight:bold;\">" + this.getName() + "<p style=\"text-indent: 10%; display : block; margin: 1%;\">"
				+ (this.getSugar() ? "w sugar" : "w/o sugar") + "</p>"
				+ (this.getComment() == null ? "" : "<p style=\"text-indent: 10%; display : block; margin: 1%;\">" + this.getComment() + "</p>");
		return s;
	}
	
	public String Meats.cookerInformations(){
		String s = "<p style=\"text-align:left; color: black; display:inline-block; padding-top:3%; width:70%; font-weight:bold;\">" + this.getName() 
				+ "<p style=\"display : block; margin: 1%;\"><p  style=\"text-indent: 10%; display : block; margin: 1%;\">" 
				+ this.getCooking()
				+ (this.getSauce() == Sauce.NONE ? "" : " " + this.getSauce()) + "</p>"
				+ (this.getComment() == null ? "" : "<p style=\"text-indent: 10%; display : block; margin: 1%;\">" + this.getComment() + "</p>");
		return s;
	}
	
	public String Food.cookerInformations(){
		String s = "<p style=\"text-align:left; color: black; display:inline-block; padding-top:3%; width:70%; font-weight:bold;\">" + this.getName() + "<p style=\"display : block; margin: 1%;\">"
				+ (this.getSauce() == Sauce.NONE ? "" : "<p style=\"text-indent: 10%; display : block; margin: 1%;\">" + this.getSauce() + "</p>")
				+ (this.getComment() == null ? "" : "<p style=\"text-indent: 10%; display : block; margin: 1%;\">" + this.getComment() + "</p>");
		return s;
	}
	
	pointcut initializeState(Prototype p, String _name, double _price, int _target, String _comment) : execution(Prototype.new(String, double, int, String)) && this(p) && args(_name, _price, _target, _comment);

	after(Prototype p, String _name, double _price, int _target, String _comment) : initializeState(p, _name, _price, _target, _comment){
		p.state = STATE.FREE;
	}
}
