import java.math.BigDecimal;

import menuqac.Order;
import menuqac.Table;

public aspect TableAspect {

	private BigDecimal Table.totalPrice;
	
	public double Table.getTotalPrice(){
		return this.totalPrice.doubleValue();
	}
	
	public void Table.addTotalPrice(BigDecimal p){
		this.totalPrice = this.totalPrice.add(p);
	}
	
	pointcut newPrice(Table t, int i) : execution(Table.new(int)) && this(t) && args(i);
	
	after(Table t, int i) : newPrice(t, i){
		t.totalPrice = BigDecimal.valueOf(0);
	}
	
	pointcut updatePrice(Table t, Order o) : execution(void Table.addOrder(Order)) && this(t) && args(o);
	
	before(Table t, Order o) : updatePrice(t, o){
		t.addTotalPrice(o.getPrice());
	}
}
