import java.util.ArrayList;

import menuqac.Payment;
import menuqac.Payment.Method;
import menuqac.Table;
import menuqac.WebController;

public aspect PaymentAspect {

//	@RequestMapping(value = "/payment")
//	public String WebController.payment(@RequestParam(required = false) double somme, @RequestParam(required=false) String moyenDePaiement) {
//		this.getSelectedTable().decreasePrice(BigDecimal.valueOf(somme));
//		System.out.println(moyenDePaiement);
//		System.out.println("paiement");
//		return "redirect:/cashRegister";
//	}
	
	private ArrayList<Payment> Table.payments = new ArrayList<Payment>();
	
	public void Table.addPayment(Payment p){
		this.payments.add(p);
	}
	
	public ArrayList<Payment> Table.getPayments(){
		return this.payments;
	}
	
	pointcut addPayment(WebController controller, double somme, String moyenDePaiement) : execution(String WebController.payment(double, String)) && this(controller) && args(somme, moyenDePaiement);
	
	after(WebController controller, double somme, String moyenDePaiement) : addPayment(controller, somme, moyenDePaiement){
		Table table = controller.getSelectedTable();
		Method m = null;
		switch(moyenDePaiement) {
		case "CB":
			m = Method.CB;
			break;
		case "esp":
			m = Method.ESPECE;
			break;
		case "che":
			m = Method.CHEQUE;
			break;
		}
		table.addPayment(new Payment(somme, m));
	}
}
