import java.math.BigDecimal;

import menuqac.Menu;
import menuqac.Table;
import menuqac.menu.Section;

public aspect SectionAspect {
	
	private String Section.fullyParentName;
	
	public void Section.setFullyParentName(String s){
		this.fullyParentName = s;
	}
	
	public String Section.getFullyParentName(){
		return this.fullyParentName;
	}

	pointcut initialisation(Section section, String parent, String name, String prototype) : execution(Section.new(String, String, String)) && this(section) && args(parent, name, prototype);

	after(Section section, String parent, String name, String prototype) : initialisation(section, parent, name, prototype){
		if(parent.equals("Drinks")) {
			section.setFullyParentName("menuqac.prototype.drink."+name.replaceAll("\\s+",""));
		}
		else {
			section.setFullyParentName("menuqac.prototype.food."+parent.replaceAll("\\s+","").substring(0, 1).toLowerCase() + parent.replaceAll("\\s+","").substring(1)+"." +name.replaceAll("\\s+",""));
		}
	}
}
