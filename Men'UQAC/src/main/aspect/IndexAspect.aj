import java.util.List;

import menuqac.app.Application;
import menuqac.app.Waiter;

privileged aspect IndexAspect {

	public List<Waiter> Application.getWaiters(){
		return this.waiters;
	}
}
