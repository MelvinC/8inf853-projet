import java.util.ArrayList;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import menuqac.WebController;
import menuqac.app.Cooker;
import menuqac.menu.Section;

public aspect WebControllerAspect {

//	pointcut index() : execution(ModelAndView WebController.index());
//	
//	after() : index(){
//		System.out.println("HELOOOOOOOOOOOOO");
//	}
		
	@RequestMapping(value = "/ticket")
	public ModelAndView WebController.ticket() {
		ModelAndView mav = new ModelAndView("ticket");
		mav.addObject("table", this.getSelectedTable());
	    return mav;
	}
	
	@RequestMapping(value = "/cooker")
	public ModelAndView WebController.cooker() {
		ModelAndView mav = new ModelAndView("cooker");
		mav.addObject("cooker", new Cooker(this.getApp().getMenu()));
		mav.addObject("menu", this.getApp().getMenu());
		mav.addObject("prototypes", this.getApp().getPrototypesBySections(this.getApp().getMenu().getSections()));
	    return mav;
	}
}
