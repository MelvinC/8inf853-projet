package menuqac;

public class Payment {
	
	public enum Method{
		CB, ESPECE, CHEQUE
	}

	private double amount;
	private Method method;
	
	public Payment(double amount, Method method) {
		this.amount = amount;
		this.method = method;
	}
	
	public double getAmount() {
		return this.amount;
	}
	
	public Method getMethod() {
		return this.method;
	}
	
	public String getMethodName() {
		return this.method.name();
	}
}
