package menuqac.app;

import java.util.ArrayList;
import java.util.List;

import menuqac.Menu;
import menuqac.Order;
import menuqac.Prototype;
import menuqac.Table;
import menuqac.menu.Section;
import menuqac.visitor.CommentVisitor;
import menuqac.visitor.CookingVisitor;
import menuqac.visitor.IceCubesVisitor;
import menuqac.visitor.SauceVisitor;
import menuqac.visitor.SauceVisitor.Sauce;
import menuqac.visitor.SugarVisitor;
import menuqac.visitor.CookingVisitor.Cooking;

import menuqac.prototype.*;
import menuqac.prototype.drink.ColdDrinks;

public class Application {

	private List<Waiter> waiters;
	private ArrayList<Table> tables;
	private Menu menu;

	public Application() {
		this.menu = new Menu();
		this.tables = new ArrayList<Table>();
		this.tables.add(new Table(1));
		this.tables.add(new Table(2));
		this.tables.add(new Table(3));
		this.waiters = new ArrayList<Waiter>();
		this.waiters.add(new Waiter(1, this.tables, this.menu));
	}
	
	public Menu getMenu(){
		return this.menu;
	}
	
	public Waiter getWaiter(int id) {
		for(Waiter w : waiters) {
			if(w.getNumber() == id) {
				return w;
			}
		}
		return null;
	}
	
	public void AddWaiter(int id) {
		if(this.findWaiter(id) == null)
			this.waiters.add(new Waiter(id, this.tables, this.menu));
	}
	
	public void addTable(Table t) {
		this.tables.add(t);
		for(Waiter w : this.waiters) {
			w.addTables(t);
		}
	}
	
	public void removeTable(Table t) {
		this.tables.remove(t);
		for(Waiter w : this.waiters) {
			w.removeTables(t);
		}
	}
	
	public List<Table> getTables(){
		return this.tables;
	}
	
	public void createOrder(Waiter waiter, int table, String[] orders) {
		Order o = new Order(waiter.getNumber(), table);
		for(String order : orders) {
			//System.out.println(order);
			String s = deleteMultipleModification(order);
			//System.out.println(s);
			String[] tab = s.split("£");
			String[] sections = tab[0].split(";");
			for(int i = tab.length-1; i>0; i--) {
				o.add(createPrototype(sections[0], sections[1], tab[i]));
			}
		}
		for(Table t : this.tables) {
			if(t.getNumber() == table) {
				t.addOrder(o);
			}
		}
	}
	
	public String deleteMultipleModification(String s) {
		ArrayList<String> id = new ArrayList<String>();
		String[] tab = s.split("£");
		StringBuilder sb = new StringBuilder(tab[0]);
		for(int i=tab.length-1; i>0; i--) {
			String[] proto = tab[i].split("§");
			if(!id.contains(proto[0])){
				sb.append("£");
				sb.append(tab[i]);
				id.add(proto[0]);
			}
		}
		return sb.toString();
	}
	
	public Prototype createPrototype(String _section, String proto, String modif) {
		//List<Section> sections = this.menu.getChildFromParent(parent);
		Prototype result = null;
		//System.out.println(_section);
		Section section = this.menu.getSectionFromName(_section);
		List<Prototype> prototypes = section.getPrototypes();
		for(Prototype p : prototypes) {
			if(p.getName().equals(proto))
				result = p.clone();
		}
		String[] modifications = modif.split("§");
		IceCubesVisitor ice = new IceCubesVisitor();
		ice.visit(result, modifications[1].equals("yes") ? true : false);
		SugarVisitor sugar = new SugarVisitor();
		sugar.visit(result, modifications[2].equals("yes") ? true : false);
		SauceVisitor sauce = new SauceVisitor();
		sauce.visit(result, Sauce.valueOf(modifications[3]));
		CookingVisitor cooking = new CookingVisitor();
		cooking.visit(result, Cooking.valueOf(modifications[4]));
		CommentVisitor comment = new CommentVisitor();
		comment.visit(result, modifications[5]);
		//System.out.println(result.toString());
		return result;
	}
	
	public Waiter findWaiter(int id) {
		for (Waiter w : this.waiters) {
			if(w.getNumber() == id) {
				return w;
			}
		}
		return null;
	}
	
	public Table findTable(int id) {
		for (Table t : this.tables) {
			if(t.getNumber() == id) {
				return t;
			}
		}
		return null;
	}
	
	public List<Prototype> getPrototypesBySections(List<Section> sections){
		ArrayList<Prototype> result = new ArrayList<Prototype>();
		for(Table t : this.getTables()) {
			for(Prototype p : t.getPrototypes()) {
				for(Section s : sections) {
					try {
						if(Class.forName(s.getFullyParentName()).isInstance(p)) {
							result.add(p);
						}
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return result;
	}
}
