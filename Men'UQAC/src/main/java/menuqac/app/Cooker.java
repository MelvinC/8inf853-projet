package menuqac.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import menuqac.Menu;
import menuqac.menu.Section;

public class Cooker {

	private Menu menu;
	private List<Section> specialities;
	
	public Cooker(Menu menu) {
		this.menu = menu;
		this.specialities = menu.getSections();
	}
	
	public Menu getMenu() {
		return this.menu;
	}
	
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	
	public List<Section> getSpecialities(){
		return this.specialities;
	}
	
	public void setSpecialities(Section[] specialities) {
		this.specialities =  specialities == null ? new ArrayList<Section>() :(ArrayList<Section>) Arrays.asList(specialities);
	}
	
	public void setSpecialities(String[] specialities) {
		this.specialities = new ArrayList<Section>();
		for(String s : specialities) {
			this.specialities.add(this.menu.getSectionFromName(s));
		}
	}
	
	public boolean contains(String s) {
		return this.specialities.contains(this.menu.getSectionFromName(s));
	}
}
