package menuqac.menu;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import menuqac.Prototype;
import menuqac.prototype.drink.ColdDrinks;
import menuqac.prototype.drink.HotDrinks;
import menuqac.prototype.food.deserts.UniqueSize;
import menuqac.prototype.food.deserts.VariableSize;
import menuqac.prototype.food.mainDishes.Commons;
import menuqac.prototype.food.mainDishes.Meats;
import menuqac.prototype.food.starters.ColdStarters;
import menuqac.prototype.food.starters.HotStarters;

public class Section implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String parent;
	private List<Prototype> prototypes;

	public Section(String parent, String name, String prototype) {
		this.name = name;
		this.parent = parent;
		this.prototypes = new LinkedList<Prototype>();
		this.parsePrototype(prototype);
	}

	public void addPrototype(Prototype p) {
		this.prototypes.add(p);
	}

	public void parsePrototype(String s) {
		String[] tab = s.split(",");
		for (String str : tab) {
			String[] proto = str.split(":");
			this.prototypes.add(new Prototype(proto[0].trim(), Double.parseDouble(proto[1]), 0, ""));
		}
	}

	public String getParent() {
		return this.parent;
	}

	public String getName() {
		return this.name;
	}

	@SuppressWarnings("unchecked")
	public List<Prototype> getPrototypes() {
		List<Prototype> list = new LinkedList<Prototype>();
		@SuppressWarnings("rawtypes")
		HashMap<String, Class> classes = new HashMap<String, Class>();
		classes.put("Cold Starters", ColdStarters.class);
		classes.put("Hot Starters", HotStarters.class);
		classes.put("Unique Size", UniqueSize.class);
		classes.put("Variable Size", VariableSize.class);
		classes.put("Commons", Commons.class);
		classes.put("Meats", Meats.class);
		classes.put("Cold Drinks", ColdDrinks.class);
		classes.put("Hot Drinks", HotDrinks.class);
		for (int i = 0; i < this.prototypes.size(); i++) {
			@SuppressWarnings("rawtypes")
			Constructor constr = null;
			try {
				constr = classes.get(this.name)
						.getConstructor(new Class[] { String.class, double.class, int.class, String.class });
			} catch (NoSuchMethodException | SecurityException e2) {
				e2.printStackTrace();
			}
			Prototype ent = null;
			try {
				ent = (Prototype) constr
						.newInstance(new Object[] { this.prototypes.get(i).getName(), this.prototypes.get(i).getPrice().doubleValue(),
								this.prototypes.get(i).getTarget(), this.prototypes.get(i).getComment() });
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				e.printStackTrace();
			}
			if (ent != null) {
				list.add(ent);
			} else {
				list.add(this.prototypes.get(i));
			}
		}
		return list;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(parent);
		sb.append("\n");
		sb.append(name);
		sb.append("\n");
		for (Prototype p : prototypes) {
			sb.append(p.getName());
			sb.append(" -> ");
			sb.append(p.getPrice());
			sb.append("\n");
		}
		return sb.toString();
	}
}