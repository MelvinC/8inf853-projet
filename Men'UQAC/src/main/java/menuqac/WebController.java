package menuqac;
//package com.tutorialspoint.demo.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ch.qos.logback.core.recovery.ResilientSyslogOutputStream;
import menuqac.app.Application;
import menuqac.app.Cooker;
import menuqac.app.Waiter;
import menuqac.menu.Section;

@Controller
public class WebController {

	private Application app = new Application();
	private Table selectedTable = this.app.getTables().isEmpty() ? null : this.app.getTables().get(0);
	
	@RequestMapping(value = "/index")
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("app", this.getApp());
		return mav;
	}

	@RequestMapping(value = "/waiter")
	public ModelAndView connectWaiter(@RequestParam(required = false) int id) {
		Waiter w = this.getApp().getWaiter(id);
		if (w != null) {
			ModelAndView mav = new ModelAndView("waiter");
			mav.addObject("waiter", this.getApp().getWaiter(id));
			return mav;
		}
		return new ModelAndView("indexBadWaiter");
	}

	@RequestMapping(value = "/Addwaiter")
	public String Addwaiter(@RequestParam(required = false) int id) {
		this.getApp().AddWaiter(id);
		return "redirect:/index";
	}
	
	@RequestMapping(value = "/cashRegister")
	public ModelAndView cashRegister() {
		ModelAndView mav = new ModelAndView("cashRegister");
		mav.addObject("tables", this.getApp().getTables());
		mav.addObject("selectedTable", this.getSelectedTable());
		return mav;
	}

	@RequestMapping(value = "/tableChoice")
	public String cashRegisterTableChoice(@RequestParam(required = false) int table) {
		this.setSelectedTable(this.getApp().findTable(table));
		return "redirect:/cashRegister";
	}

	@RequestMapping(value = "/payment")
	public String payment(@RequestParam(required = false) double somme, @RequestParam(required=false) String moyenDePaiement) {
		this.getSelectedTable().decreasePrice(BigDecimal.valueOf(somme));
		return "redirect:/cashRegister";
	}

	@RequestMapping(value = "/flushTable")
	public String cashRegisterFlushTable() {
		this.getSelectedTable().flush();
		return "redirect:/cashRegister";
	}
	
	@RequestMapping(value = "/commande")
	public String order(@RequestParam(required = false) int waiter, @RequestParam(required = false) String table, @RequestParam(required = false) String[] order) {
		this.getApp().createOrder(this.getApp().findWaiter(waiter), Integer.valueOf(table), order);
		return "redirect:/waiter?id=" + waiter;
	}
	
	@RequestMapping(value = "/cookerUpdate")
	public ModelAndView cookerUpdate(@RequestParam(required = false) String[] specialities) {
		ModelAndView mav = new ModelAndView("cooker");
		Cooker cooker = new Cooker(this.app.getMenu());
		cooker.setSpecialities(specialities);
		mav.addObject("cooker", cooker);
		mav.addObject("menu", this.app.getMenu());
		ArrayList<Section> sections = new ArrayList<Section>();
		for(String s : specialities) {
			sections.add(this.getApp().getMenu().getSectionFromName(s));
		}
		mav.addObject("prototypes", this.getApp().getPrototypesBySections(sections));
	    return mav;
	}
	
	@RequestMapping(value = "/cookerUpdateProcess")
	public ModelAndView cookerUpdate(@RequestParam(required = false) int prototype, @RequestParam(required = false) String[] specialities) {
		ModelAndView mav = new ModelAndView("cooker");
		Cooker cooker = new Cooker(this.app.getMenu());
		cooker.setSpecialities(specialities);
		mav.addObject("cooker", cooker);
		mav.addObject("menu", this.app.getMenu());
		this.app.getPrototypesBySections(cooker.getSpecialities()).get(prototype).switchState();
		ArrayList<Section> sections = new ArrayList<Section>();
		for(String s : specialities) {
			sections.add(this.getApp().getMenu().getSectionFromName(s));
		}
		mav.addObject("prototypes", this.getApp().getPrototypesBySections(sections));
	    return mav;
	}
	
	public Application getApp() {
		return this.app;
	}

	public Table getSelectedTable() {
		return this.selectedTable;
	}

	public void setSelectedTable(Table table) {
		this.selectedTable = table;
	}
}