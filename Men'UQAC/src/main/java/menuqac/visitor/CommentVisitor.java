package menuqac.visitor;

import menuqac.Prototype;
import menuqac.Visitor;

public class CommentVisitor implements Visitor {
	@Override
	public void visit(Prototype p, Object value) {
		p.accepts(this, (String) value);
	}
}