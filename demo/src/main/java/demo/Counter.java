package demo;

public class Counter {

	public int counter;
	
	public Counter() {
		this.counter = 0;
	}
	
	public int getCounter() {
		return this.counter;
	}
	
	public void incr() {
		this.counter++;
	}
}
