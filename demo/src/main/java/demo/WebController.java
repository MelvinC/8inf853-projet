package demo;
//package com.tutorialspoint.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebController {
	
	private Counter counter = new Counter();
	
   @RequestMapping(value = "/index")
   public ModelAndView index() {
	   ModelAndView mav = new ModelAndView("index");
       mav.addObject("counter", counter);
       return mav;
   }
   
   @RequestMapping(value = "/incrementation")
   public String incr() {
	   this.counter.incr();
       return "redirect:/index";
   }
}